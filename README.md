# It all fell

Welcome visitor from the future!

As on 11 July 2022 майнкрафт itself joined the war ( https://www.minecraft.net/en-us/article/welcome-queercraft they even did change the name grrr!!!!) so making an apolitical launcher makes no sense. So this is code archive for WoliMC, the minecraft launcher.

This code is (and was) public domain, do everything you want with this, but at your own risk. I hope you will make something useful from this.


































<p align="center">
	<img src="https://codeberg.org/glowiak/wolimc/raw/branch/master/app/src/main/resources/logo.png">
</p>

# WoliMC

Woli(Polish for 'preffers')MC (pronounce 'voly em tse') is a Minecraft launcher with support for instances that won't go into politics/ideological war. Never.

### Website

We have a website now! Visit it at <p><a href="http://glowiak2.github.io/wolimc">http://glowiak2.github.io/wolimc</a></p>. English and Polish versions are available.

### Installation

Download latest release jar and run it. Please have on mind that it's in early phase and not everything is working yet.

### Roadmap

[ X ] Downloading and launching game

[ X ] GUI

[ REMOVED ] Kat Kommander (better than a regular cat)

[ X ] Managing instances in GUI

[ ] Have all the versions in manifest

[ ] Mojang and Micro$oft authentication

[ X ] Custom JVM options

[ PARTIAL] Fetching assets (needed for music and sounds to work)

[ X ] a1.0.16.05 patches like deleting preview_data and filling v3_act on start

[ ] skins in a1.0.16.05 via Config.getCwd()/skincache directory

[ X ] Custom game code in a1.0.16.05-lilypad_qa

[ X ] Translations

[ ] Instance icons

### Translations

[X] English

[X] Polish

[X] Latin

### Screenshots

none yet

### Debugging

Normally all debug messages are hidden, but saved to the launcher.log file. If you want to see that messages in action, set the environment variable DEBUG to TRUE.

### Notes to operating systems

Window$ XP: it's ssl certs are too old to download from codeberg, so you have to manually download the indexes:

	https://codeberg.org/glowiak/wolimc/raw/branch/meta/VersionsByProtocol.txt as versions.txt
	https://codeberg.org/glowiak/wolimc/raw/branch/meta/VersionsByName.txt as version_names.txt

Window$ 10: the window's height is bit smaller, i have no idea why,

Every Window$: don't store the jar in location with a space, or game won't run

### Credits

<strike>The Kat Kommander image is a cutted screenshot from The Kid vs Kat episode 37 ( https://www.youtube.com/watch?v=APkqUAqB5ZQ&list=PL5vwZL9hcSWkF_nYqxkolFY_tXPElY3sA&index=54 )</strike> removed The Kat Kommander, as I don't want this to be dmcad

### Bla bla bla

I am not associated with Mojang nor Microsoft. Minecraft is a property of Mojang AB.
