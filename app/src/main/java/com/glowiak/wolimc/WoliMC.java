package com.glowiak.wolimc;

import com.glowiak.wolimc.DownloadFile;
import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.launch.Classpath;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.launch.Launch;
import com.glowiak.wolimc.gui.MainWindow;
import com.glowiak.wolimc.gui.NewcomerWizard;
import com.glowiak.wolimc.gui.LogViewer;

import java.util.Scanner;
import java.util.ArrayList;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

public class WoliMC
{
    public static ArrayList<String> logs = new ArrayList<String>();
    
    public static void main(String[] args)
    {
        try {
            if (System.getenv("DEBUG").equals("TRUE"))
            { Config.debug = true; } else
            { Config.debug = false; }
        } catch (NullPointerException e) { Config.debug = false; }
        
        try {
            if (System.getenv("LOGV").equals("TRUE"))
            {
                LogViewer.start();
            }
        } catch (NullPointerException ee) { log("ERROR", "WoliMC", ee.toString()); }
        
        log("INFO", "WoliMC", String.format("Starting WoliMC version %s", getVersion()));
        
        if (args.length <= 0) {
            if (!new File(String.format("%s/wizardDone", Config.getCwd())).exists())
            { NewcomerWizard.run(); } else
            { MainWindow.run(); }
        } else {
        switch(args[0])
        {
            case "createConfig":
                Config.createNewConfig();
                break;
            case "setGlobalOpt":
                if (args.length < 3) { System.out.println("Not enough arguments!"); System.exit(1); }
                new Config().setConfig(args[1], args[2]);
                break;
            case "guidedConfig":
                guidedConfig();
                break;
            case "createInstance":
                if (args.length < 3) { System.out.println("Not enough arguments!"); System.exit(1); }
                if (!new File(String.format("%s/instances", Config.getCwd())).exists())
                { Config.mkdir(String.format("%s/instances", Config.getCwd())); }
                Instance.createInstance(args[1], args[2]);
                break;
            case "listInstances":
                System.out.println(Instance.getInstanceList());
                break;
            case "deleteInstance":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                Instance.deleteInstance(args[1]);
                break;
            case "downloadJars":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                if (Instance.exists(args[1]))
                { Classpath.fetchJars(args[1]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "updateManifest":
                Version.updateVersionManifest();
                break;
            case "select":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                Config.loadConfig();
                if (Instance.exists(args[1]))
                { Config.setConfig("selinst", args[1]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "launch":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                Config.loadConfig();
                if (Instance.exists(args[1]))
                { Launch.startInstance(args[1]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "setProp":
                if (args.length < 4) { System.out.println("Not enough arguments!"); System.exit(1); }
                if (Instance.exists(args[1]))
                { Instance.setProperty(args[1], args[2], args[3]); } else
                { System.out.println("Invalid instance name."); System.exit(1); }
                break;
            case "help":
                printHelp();
                break;
            case "import":
                if (args.length < 2) { System.out.println("Not enough arguments!"); System.exit(1); }
                boolean isDone = Instance.importInstance(args[1]);
                if (!isDone)
                {
                    System.out.println("File does not exist!");
                }
                break;
        }}
    }
    public static void printHelp()
    {
        System.out.println("WoliMC is an apolitical Minecraft launcher.");
        System.out.println("Usage: $JAVA /path/to/WoliMC.jar $OPT");
        System.out.println("Options:");
        System.out.println("createConfig <null>              | create a default config file");
        System.out.println("setGlobalOpt <id> <value>        | set option");
        System.out.println("guidedConfig <null>              | guided configurator");
        System.out.println("createInstance <name> <version>  | create a new instance");
        System.out.println("deleteInstance <name>            | remove an instance");
        System.out.println("listInstances <null>             | list instances");
        System.out.println("downloadJars <name>              | download jars for selected instance");
        System.out.println("updateManifest <null>            | update version manifest");
        System.out.println("select <name>                    | select an instance");
        System.out.println("launch <name>                    | launch an instance");
        System.out.println("setProp <name> <setting> <value> | modify instance settings");
        System.out.println("import /path/to/file.zip         | import an instance");
        System.out.println("");
        System.out.println("You can also pass environment variables like so:");
        System.out.println("DEBUG <TRUE/FALSE>      | view log in terminal (FALSE by default)");
        System.out.println("LOGV <TRUE/FALSE>       | launch a graphical log viewer (FALSE by default)");
    }
    public static void guidedConfig()
    {
        if (new File(String.format("%s/wolimc.cfg", Config.getCwd())).exists()) { new File(String.format("%s/wolimc.cfg", Config.getCwd())).delete(); }
        
        try {
            FileWriter fw = new FileWriter(String.format("%s/wolimc.cfg", Config.getCwd()));
            fw.write("This is WoliMC config file\n");
            System.out.print("Java path: ");
            String readm_java = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_java));
        
            System.out.print("Memory: ");
            String readm_mem = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_mem));
            
            System.out.print("Meta server: ");
            String readm_meta = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_meta));
            
            fw.write("default\n");
            
            System.out.print("Nick: ");
            String readm_nick = new Scanner(System.in).next();
            fw.write(String.format("%s\n", readm_nick));
            
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static String getVersion()
    {
        return String.format("%d.%d.%d-%s", Config.WoliMC_VERSION_MAJOR, Config.WoliMC_VERSION_MINOR, Config.WoliMC_VERSION_HOTFIX, Config.WoliMC_CODE_BRANCH);
    }
    public static String getTime()
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");  
        LocalDateTime now = LocalDateTime.now();  
        return dtf.format(now);  
    }
    public static void log(String type, String classN, String message)
    {
        String logMsg = String.format("[%s] [%s/%s] %s", getTime(), classN, type, message);
        logs.add(logMsg);
        if (Config.debug)
        { System.out.println(logMsg); }
        try {
            FileWriter fw = new FileWriter(String.format("%s/launcher.log", Config.getCwd()));
            for (int i = 0; i < logs.size(); i++)
            {
                fw.write(String.format("%s\n", logs.get(i)));
            }
            fw.close();
        } catch (IOException ioe) { log("INFO", "WoliMC", ioe.toString()); }
    }
}
