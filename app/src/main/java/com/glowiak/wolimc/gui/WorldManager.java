package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import java.awt.Image;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.io.IOException;
import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.gui.MainWindow;
import com.glowiak.wolimc.lang.Translations;
import com.glowiak.wolimc.WoliMC;

interface MWI
{
    JFrame w = new JFrame(String.format("%s %s", Instance.getSelected(), Translations.getTranslation("Worlds")));
    JButton w_del = new JButton(Translations.getTranslation("Remove"));
    JButton w_exp = new JButton(Translations.getTranslation("Export"));
    JButton w_imp = new JButton(Translations.getTranslation("Import"));
    ButtonGroup bgm = new ButtonGroup();
    JRadioButton[] rbm = new JRadioButton[50];
}

public class WorldManager implements MWI
{
    public static void run()
    {
        draw(400,500);
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        w_del.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                WoliMC.log("INFO", "WorldManager", String.format("Remove world %s", MainWindow.getSelectedButtonText(bgm)));
                rmdir(new File(String.format("%s/instances/%s/.minecraft/saves/%s", Config.getCwd(), Instance.getSelected(), MainWindow.getSelectedButtonText(bgm))));
            }
        });
    }
    public static void draw(int width, int height)
    {
        w.setSize(width, height);
        
        int base_x = 5;
        int base_y = 5;
        
        w_del.setBounds(width - 110, 0, 100, 25);
        w_exp.setBounds(width - 110, 25, 100, 25);
        w_imp.setBounds(width - 110, 50, 100, 25);
        
        if (new File(String.format("%s/instances/%s/.minecraft/saves", Config.getCwd(), Instance.getSelected())).exists()) {
        
            File[] fmm = new File(String.format("%s/instances/%s/.minecraft/saves", Config.getCwd(), Instance.getSelected())).listFiles();
            for (int i = 0; i < fmm.length; i++)
            {
                rbm[i] = new JRadioButton();
                rbm[i].setText(Instance.basename(fmm[i].toString()));
                if (new File(String.format("%s/instances/%s/.minecraft/saves/%s/icon.png", Config.getCwd(), Instance.getSelected(), Instance.basename(fmm[i].toString()))).exists())
                {
                    rbm[i].setIcon(getScaledImage(String.format("%s/instances/%s/.minecraft/saves/%s/icon.png", Config.getCwd(), Instance.getSelected(), Instance.basename(fmm[i].toString())), 25, 25));
                }
                rbm[i].setBounds(base_x, base_y, 250, 25);
                bgm.add(rbm[i]);
                w.add(rbm[i]);
                base_y += 25;
            }
        }
        
        w.add(w_del);
        w.add(w_exp);
        w.add(w_imp);
        
        w.setLayout(null);
        w.setVisible(true);
    }
    public static boolean rmdir(File dir) { // based on https://www.baeldung.com/java-delete-directory
        WoliMC.log("INFO", "WorldManager", String.format("DELETE %s", dir.toString()));
        File[] allContents = dir.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                rmdir(file);
            }
        }
        return dir.delete();
    }
    public static ImageIcon getScaledImage(String imgpath, int width, int height)
    {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(imgpath));
        } catch (IOException ioe) { System.out.println(ioe); }
        
        Image jmg = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        return new ImageIcon(jmg);
    }
}
