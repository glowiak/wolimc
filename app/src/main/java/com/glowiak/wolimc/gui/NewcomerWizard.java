package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.gui.MainWindow;
import com.glowiak.wolimc.lang.Translations;
import com.glowiak.wolimc.WoliMC;

interface NWI
{
    JFrame w = new JFrame("Wizard");
    JLabel tl = new JLabel();
    JButton b_next = new JButton("Next");
    JTextField tf = new JTextField();
    JButton jb = new JButton("Browse");
    
    // LANGUAGE BUTTONS
    JRadioButton LB_EN = new JRadioButton("English");
    JRadioButton LB_PL = new JRadioButton("Polski");
    JRadioButton LB_LA = new JRadioButton("Latina");
    
    ButtonGroup bg = new ButtonGroup();
}

public class NewcomerWizard implements NWI
{
    public static int step = 0;
    
    public static String __JAVA = "";
    public static String __NICK = "";
    public static String __MEMORY = "";
    public static String __LANG = "";
    
    public static void run()
    {
        WoliMC.log("INFO", "NewcomerWizard", "Starting wizard dialog");
        Config.language = "english";
        draw(500, 600);
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        
        b_next.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                WoliMC.log("INFO", "NewcomerWizard", String.format("Entering step %d", (step + 1)));
                if (!tf.getText().contains("java") || !tf.getText().contains("runtime") && step == 2)
                {
                    JOptionPane.showMessageDialog(w, Translations.getTranslation("ProperJava"));
                } else if (tf.getText().isEmpty() && step == 3)
                {
                    JOptionPane.showMessageDialog(w, Translations.getTranslation("ProperNick"));
                } else
                {
                    if (step == 0)
                    {
                        if (LB_EN.isSelected())
                        {
                            __LANG = "english";
                            Config.language = "english";
                        }
                        else if (LB_PL.isSelected())
                        {
                            __LANG = "polski";
                            Config.language = "polski";
                        }
                        else if (LB_LA.isSelected())
                        {
                            __LANG = "latina";
                            Config.language = "latina";
                        }
                        else if (!LB_EN.isSelected() && !LB_PL.isSelected() && !LB_LA.isSelected())
                        {
                            __LANG = "english";
                            Config.language = "english";
                        }
                        
                        w.setTitle(Translations.getTranslation("WIZARD_TITLE"));
                        b_next.setText(Translations.getTranslation("Next"));
                        jb.setText(Translations.getTranslation("Browse"));
                    } else if (step == 2)
                    {
                        __JAVA = tf.getText();
                    } else if (step == 3)
                    {
                        __NICK = tf.getText();
                    } else if (step == 4)
                    {
                        __MEMORY = tf.getText();
                    }
                    tf.setText("");
                    step++;
                    if (step == 4)
                    {
                        tf.setText("256M");
                    }
                    if (step >= 6)
                    {
                        try {
                            FileWriter fw = new FileWriter(String.format("%s/wolimc.cfg", Config.getCwd()));
                        
                            fw.write("This is WoliMC config file\n");
                            fw.write(String.format("%s\n", __JAVA));
                            fw.write(String.format("%s\n", __MEMORY));
                            fw.write("https://codeberg.org/glowiak/wolimc/raw/branch/meta\n");
                            fw.write("default\n");
                            fw.write(String.format("%s\n", __NICK));
                        
                            fw.close();
                            
                            fw = new FileWriter(String.format("%s/language", Config.getCwd()));
                            fw.write(String.format("%s\n", __LANG));
                            fw.close();
                            
                            new File(String.format("%s/wizardDone", Config.getCwd())).createNewFile();
                            
                            w.dispose();
                            MainWindow.run();
                        } catch (IOException ioe) { System.out.println(ioe); }
                    }
                }
            }
        });
        jb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int jfr = jfc.showOpenDialog(w);
                if (jfr == JFileChooser.APPROVE_OPTION)
                {
                    tf.setText(jfc.getSelectedFile().getPath());
                }
            }
        });
        
        while (true)
        {
            switch (step)
            {
                case 1:
                    LB_EN.setVisible(false);
                    LB_PL.setVisible(false);
                    LB_LA.setVisible(false);
                    tf.setVisible(false);
                    jb.setVisible(false);
                    tl.setText(Translations.getTranslation("WelcomeNew"));
                    break;
                case 0:
                    LB_EN.setVisible(true);
                    LB_PL.setVisible(true);
                    LB_LA.setVisible(true);
                    tf.setVisible(false);
                    jb.setVisible(false);
                    tl.setText("");
                    break;
                case 2:
                    LB_EN.setVisible(false);
                    LB_PL.setVisible(false);
                    LB_LA.setVisible(false);
                    tf.setVisible(true);
                    jb.setVisible(true);
                    tl.setText(Translations.getTranslation("WIZARD_JAVA"));
                    break;
                case 3:
                    jb.setVisible(false);
                    tf.setVisible(true);
                    tl.setText(Translations.getTranslation("WIZARD_NICK"));
                    break;
                case 4:
                    jb.setVisible(false);
                    tf.setVisible(true);
                    tl.setText(Translations.getTranslation("WIZARD_MEMORY"));
                    break;
                case 5:
                    jb.setVisible(false);
                    tf.setVisible(false);
                    tl.setText(Translations.getTranslation("WIZARD_THANKYOU"));
                    break;
            }
        }
    }
    public static void draw(int width, int height)
    {
        w.setSize(width, height);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        bg.add(LB_EN);
        bg.add(LB_PL);
        bg.add(LB_LA);

        b_next.setBounds(width - 105, height - 55, 100, 25);
        tl.setBounds(10, 1, width - 10, height - 200);
        tf.setBounds(0, height - 150, width - 105, 25);
        jb.setBounds(width - 105, height - 150, 100, 25);
        LB_EN.setBounds(10, 10, 150, 25);
        LB_PL.setBounds(10, 35, 150, 25);
        LB_LA.setBounds(10, 60, 150, 25);
        
        w.add(b_next);
        w.add(tl);
        w.add(tf);
        w.add(jb);
        w.add(LB_EN);
        w.add(LB_PL);
        w.add(LB_LA);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
