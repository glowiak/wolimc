package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.DownloadFile;
import com.glowiak.wolimc.lang.Translations;
import com.glowiak.wolimc.WoliMC;

interface IOV
{
    JFrame w = new JFrame(String.format("%s %s", Instance.getSelected(), Translations.getTranslation("Options")));
    JLabel replace_warn = new JLabel(Translations.getTranslation("ReplaceWarn"));
    JButton applyb = new JButton(Translations.getTranslation("Apply"));
    JLabel text_ver = new JLabel(Translations.getTranslation("Version1"));
    JLabel text_java = new JLabel(Translations.getTranslation("JavaPath"));
    JLabel text_mem = new JLabel(Translations.getTranslation("Memory"));
    JLabel text_nick = new JLabel(Translations.getTranslation("Nick"));
    JLabel text_class = new JLabel(Translations.getTranslation("MainClass"));
    JTextField tfj = new JTextField();
    JTextField tfm = new JTextField();
    JTextField tfn = new JTextField();
    JTextField tfmc = new JTextField();
    JComboBox cbv = new JComboBox();
    JButton cj = new JButton(Translations.getTranslation("Browse"));
    JLabel text_jvmargs = new JLabel(Translations.getTranslation("JVMArgs"));
    JTextField jvmargs = new JTextField();
    JButton butt_defc = new JButton(Translations.getTranslation("UseAutoClass"));
}

public class InstanceOptions implements IOV
{
    public static void run()
    {
        WoliMC.log("INFO", "InstanceOptions", String.format("Starting instance options dialog for %s", Instance.getSelected()));
        draw(400, 300);
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        applyb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                WoliMC.log("INFO", "InstanceOptions", String.format("Save settings for %s", Instance.getSelected()));
                Config.loadConfig();
                
                try {
                    FileWriter fw = new FileWriter(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), Instance.getSelected()));
                    
                    if (cbv.getSelectedItem().toString() != Instance.getVersion(Instance.getSelected()))
                    {
                        if (new File(String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), Instance.getSelected())).exists())
                        {
                            new File(String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), Instance.getSelected())).delete();
                        }
                        DownloadFile.fetch(Version.getClientUrl(cbv.getSelectedItem().toString()), String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), Instance.getSelected()));
                        fw.write(String.format("%s\n", cbv.getSelectedItem().toString()));
                    } else
                    { fw.write(String.format("%s\n", Instance.getVersion(Instance.getSelected()))); }
                    fw.write(String.format("%s\n", tfj.getText()));
                    fw.write(String.format("%s\n", tfm.getText()));
                    fw.write(String.format("%s\n", tfn.getText()));
                    fw.write(String.format("%s\n", tfmc.getText()));
                    
                    fw.close();
                    Instance.setJvmArgs(Instance.getSelected(), jvmargs.getText());
                } catch (IOException ioe) { System.out.println(ioe); }
                
                Config.loadConfig();
                
                w.dispose();
            }
        });
        cj.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int oc = jfc.showOpenDialog(IOV.w);
                if (oc == JFileChooser.APPROVE_OPTION)
                {
                    tfj.setText(jfc.getSelectedFile().getPath());
                }
            }
        });
        try {
            if (!new File(String.format("%s/version_names.txt", Config.getCwd())).exists())
            { Version.updateVersionManifest(); }
            
            FileReader fr = new FileReader(String.format("%s/version_names.txt", Config.getCwd()));
            BufferedReader br = new BufferedReader(fr);
            
            cbv.addItem(Instance.getVersion(Instance.getSelected()));
            
            String s;
            while ((s = br.readLine()) != null)
            {
                if (s != Instance.getVersion(Instance.getSelected()))
                { cbv.addItem(s); }
            }
            
            br.close();
            fr.close();
            
        } catch (IOException ioe) { System.out.println(ioe); }
        butt_defc.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                tfmc.setText(Version.getAutoClass(cbv.getSelectedItem().toString()));
            }
        });
    }
    public static void draw(int width, int height)
    {
        Config.loadConfig();
        w.setSize(width, height);
        
        replace_warn.setBounds(5, 5, 350,25);
        applyb.setBounds(width - 105, height - 55, 100, 25);
        text_ver.setBounds(5, 25, 100, 25);
        text_java.setBounds(5, 50, 100, 25);
        text_mem.setBounds(5, 75, 100, 25);
        text_nick.setBounds(5, 100, 100, 25);
        text_class.setBounds(5, 125, 100, 25);
        tfj.setBounds(100, 50, width - 200, 25);
        tfm.setBounds(100, 75, width - 100, 25);
        tfn.setBounds(100, 100, width - 100, 25);
        tfmc.setBounds(100, 125, width - 100, 25);
        cbv.setBounds(100, 25, width - 200, 25);
        cj.setBounds(width - 100, 50, 100, 25);
        text_jvmargs.setBounds(5, 150, 100, 25);
        jvmargs.setBounds(100, 150, width - 100, 25);
        butt_defc.setBounds(0, 175, 200, 25);
        
        tfj.setText(Instance.getJava(Instance.getSelected()));
        tfm.setText(Instance.getMemory(Instance.getSelected()));
        tfn.setText(Instance.getNick(Instance.getSelected()));
        tfmc.setText(Instance.getMainClass(Instance.getSelected()));
        jvmargs.setText(Instance.getJvmArgs(Instance.getSelected()));
        
        w.add(replace_warn);
        w.add(applyb);
        w.add(text_ver);
        w.add(text_java);
        w.add(text_mem);
        w.add(text_nick);
        w.add(text_class);
        w.add(tfj);
        w.add(tfm);
        w.add(tfn);
        w.add(tfmc);
        w.add(cbv);
        w.add(cj);
        w.add(text_jvmargs);
        w.add(jvmargs);
        w.add(butt_defc);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
