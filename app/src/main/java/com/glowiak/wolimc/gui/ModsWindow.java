package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;

import java.awt.Component;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.glowiak.wolimc.gui.MainWindow;
import com.glowiak.wolimc.gui.WorldManager;
import com.glowiak.wolimc.Unzip;
import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.lang.Translations;
import com.glowiak.wolimc.launch.Launch;
import com.glowiak.wolimc.WoliMC;

import java.io.File;
import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

interface MWW
{
    JFrame w = new JFrame(String.format("%s %s", Instance.getSelected(), Translations.getTranslation("Mods")));
    JButton inst_modl = new JButton(Translations.getTranslation("InstallModloader"));
    JButton inst_forge = new JButton(Translations.getTranslation("InstallForge"));
    JButton inst_fabric = new JButton(Translations.getTranslation("InstallFabric"));
    JButton inst_jar = new JButton(Translations.getTranslation("AddToJar"));
    JButton inst_mod = new JButton(Translations.getTranslation("AddMod"));
    JButton rem_mod = new JButton(Translations.getTranslation("RemoveMod"));
}

public class ModsWindow implements MWW
{
    public static void run()
    {
        WoliMC.log("INFO", "ModsWindow", "Starting mods dialog");
        draw(600, 500);
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        inst_jar.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int jfr = jfc.showOpenDialog(w);
                if (jfr == JFileChooser.APPROVE_OPTION)
                {
                    String out = Config.getCwd() + "/jarmods/" + Instance.getSelected() + "/" + Instance.basename(jfc.getSelectedFile().getPath());
                    if (!new File(Config.getCwd() + "/jarmods/" + Instance.getSelected()).exists()) { new File(Config.getCwd() + "/jarmods/" + Instance.getSelected()).mkdirs(); }
                    try {
                        Files.copy(Paths.get(jfc.getSelectedFile().getPath()), Paths.get(out), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException ioe) { System.out.println(ioe); }
                }
            }
        });
    }
    public static void draw(int width, int height)
    {
        w.setSize(width, height);
        
        inst_modl.setBounds(width - 175, 0, 170, 25);
        inst_forge.setBounds(width - 175, 25, 170, 25);
        inst_fabric.setBounds(width - 175, 50, 170, 25);
        inst_jar.setBounds(width - 175, 75, 170, 25);
        inst_mod.setBounds(width - 175, 100, 170, 25);
        rem_mod.setBounds(width - 175, 125, 170, 25);
        
        w.add(inst_modl);
        w.add(inst_forge);
        w.add(inst_fabric);
        w.add(inst_jar);
        w.add(inst_mod);
        w.add(rem_mod);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
