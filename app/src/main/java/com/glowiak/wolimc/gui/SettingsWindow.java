package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.lang.Translations;
import com.glowiak.wolimc.WoliMC;

interface SWB
{
    JFrame w = new JFrame(Translations.getTranslation("Settings"));
    JButton applyb = new JButton(Translations.getTranslation("Apply"));
    JLabel text_javap = new JLabel(Translations.getTranslation("JavaPath"));
    JTextField tf = new JTextField();
    JLabel text_mem = new JLabel(Translations.getTranslation("Memory"));
    JTextField tfm = new JTextField();
    JLabel text_ms = new JLabel(Translations.getTranslation("Metaserver"));
    JTextField tfms = new JTextField();
    JLabel text_nick = new JLabel(Translations.getTranslation("Nick"));
    JTextField tfn = new JTextField();
    JButton selj = new JButton(Translations.getTranslation("Browse"));
    JLabel text_code = new JLabel(Translations.getTranslation("LilypadQA"));
    JTextField tfc = new JTextField();
    JLabel text_lang = new JLabel(Translations.getTranslation("Language"));
    JComboBox cb_lang = new JComboBox();
}

public class SettingsWindow implements SWB
{
    public static void run()
    {
        WoliMC.log("INFO", "SettingsWindow", "Starting settings dialog");
        draw(400,275);
        
        cb_lang.removeAllItems();
        cb_lang.addItem(Config.language);
        if (!Config.language.equals("polski")) { cb_lang.addItem("polski"); }
        if (!Config.language.equals("english")) { cb_lang.addItem("english"); }
        if (!Config.language.equals("latina")) { cb_lang.addItem("latina"); }
        
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        
        applyb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                WoliMC.log("INFO", "SettingsWindow", "Save settings");
                Config.loadConfig();
                
                try {
                    FileWriter fw = new FileWriter(String.format("%s/wolimc.cfg", Config.getCwd()));
                    
                    fw.write("This is WoliMC config file\n");
                    fw.write(String.format("%s\n", tf.getText()));
                    fw.write(String.format("%s\n", tfm.getText()));
                    fw.write(String.format("%s\n", tfms.getText()));
                    fw.write(String.format("%s\n", Config.setting_selinst));
                    fw.write(String.format("%s\n", tfn.getText()));
                    
                    fw.close();
                    
                    Config.language = cb_lang.getSelectedItem().toString();
                    fw = new FileWriter(String.format("%s/language", Config.getCwd()));
                    fw.write(String.format("%s\n", cb_lang.getSelectedItem().toString()));
                    fw.close();
                    WMB.w.repaint();
                    Config.setLilypadId(tfc.getText());
                } catch (IOException ioe) { System.out.println(ioe); }
                
                Config.loadConfig();
                
                w.dispose();
            }
        });
        selj.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int oc = jfc.showOpenDialog(SWB.w);
                if (oc == JFileChooser.APPROVE_OPTION)
                {
                    tf.setText(jfc.getSelectedFile().getPath());
                }
            }
        });
    }
    public static void draw(int width, int height)
    {
        Config.loadConfig();
        w.setSize(width, height);
        
        applyb.setBounds(width - 114, height - 55, 100, 25);
        text_javap.setBounds(10, 25, 100, 25);
        tf.setBounds(110, 25, width - 210, 25);
        text_mem.setBounds(10, 50, 100, 25);
        tfm.setBounds(110, 50, width - 100, 25);
        text_ms.setBounds(10, 75, 100, 25);
        tfms.setBounds(110, 75, width - 100, 25);
        text_nick.setBounds(10, 100, 100, 25);
        tfn.setBounds(110, 100, width - 100, 25);
        selj.setBounds(width - 100, 25, 100, 25);
        text_code.setBounds(10, 125, 100, 25);
        tfc.setBounds(110, 125, width - 100, 25);
        text_lang.setBounds(10, 150, 100, 25);
        cb_lang.setBounds(110, 150, width - 100, 25);
        
        tf.setText(Config.setting_java);
        tfm.setText(Config.setting_mem);
        tfms.setText(Config.setting_meta);
        tfn.setText(Config.setting_nick);
        tfc.setText(Config.getLilypadId());
        
        w.add(applyb);
        w.add(text_javap);
        w.add(tf);
        w.add(text_mem);
        w.add(tfm);
        w.add(text_ms);
        w.add(tfms);
        w.add(text_nick);
        w.add(tfn);
        w.add(selj);
        w.add(text_code);
        w.add(tfc);
        w.add(text_lang);
        w.add(cb_lang);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
