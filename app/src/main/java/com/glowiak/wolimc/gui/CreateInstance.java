package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.lang.Translations;
import com.glowiak.wolimc.WoliMC;

interface SCI
{
    JFrame w = new JFrame(Translations.getTranslation("AddInstance"));
    JButton cb = new JButton(Translations.getTranslation("Create"));
    JLabel text_nm = new JLabel(Translations.getTranslation("Name"));
    JTextField instNam = new JTextField();
    JLabel text_ver = new JLabel(Translations.getTranslation("Version1"));
    JComboBox sel_ver = new JComboBox();
    JLabel text_class = new JLabel(Translations.getTranslation("MainClass"));
    JTextField tfmc = new JTextField();
    JLabel classWarn = new JLabel(Translations.getTranslation("ClassWarn"));
    JButton butt_defc = new JButton(Translations.getTranslation("UseAutoClass"));
}

public class CreateInstance implements SCI
{
    public static void run()
    {
        WoliMC.log("INFO", "CreateInstance", "Starting create instance dialog");
        draw(450, 250);
        instNam.setText("");
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        try {
            if (!new File(String.format("%s/version_names.txt", Config.getCwd())).exists())
            { Version.updateVersionManifest(); }
            
            FileReader fr = new FileReader(String.format("%s/version_names.txt", Config.getCwd()));
            BufferedReader br = new BufferedReader(fr);
            
            String s;
            while ((s = br.readLine()) != null)
            {
                sel_ver.addItem(s);
            }
            
            br.close();
            fr.close();
        } catch (IOException ioe) { System.out.println(ioe); }
        cb.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if (instNam.getText() == null || instNam.getText().equals("") || instNam.getText().equals(" "))
                {
                    JOptionPane.showMessageDialog(WMB.w, Translations.getTranslation("INVALID_NAME"));
                } else
                {
                    WoliMC.log("INFO", "CreateInstance", String.format("Create instance %s", instNam.getText()));
                    Instance.createInstance(instNam.getText(), sel_ver.getSelectedItem().toString());
                    Instance.setProperty(instNam.getText(), "main-class", tfmc.getText());
                    Config.setConfig("selinst", instNam.getText());
                    MainWindow.draw(MainWindow.globalWidth, MainWindow.globalHeight);
                    WMB.w.repaint();
                    w.dispose();
                }
            }
        });
        butt_defc.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                tfmc.setText(Version.getAutoClass(sel_ver.getSelectedItem().toString()));
            }
        });
    }
    public static void draw(int width, int height)
    {
        w.setSize(width, height);
        
        cb.setBounds(width - 116, height - 55, 100, 25);
        text_nm.setBounds(10, 25, 100, 25);
        instNam.setBounds(100, 25, width - 100, 25);
        text_ver.setBounds(10, 50, 100, 25);
        sel_ver.setBounds(100, 50, width - 200, 25);
        text_class.setBounds(10, 75, 100, 25);
        tfmc.setBounds(100, 75, width - 100, 25);
        classWarn.setBounds(5, height - 85, width - 5, 25);
        butt_defc.setBounds(0, 100, 200, 25);
        
        tfmc.setText(Version.mainClass);
        
        w.add(cb);
        w.add(text_nm);
        w.add(instNam);
        w.add(text_ver);
        w.add(sel_ver);
        w.add(text_class);
        w.add(tfmc);
        w.add(classWarn);
        w.add(butt_defc);
        
        w.setLayout(null);
        w.setVisible(true);
    }
}
