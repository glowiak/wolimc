package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JFileChooser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.awt.Component;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.glowiak.wolimc.WoliMC;

// this gui is not translated, as the log itself is in english

public class LogViewer implements Runnable
{
    static int globalWidth = 800;
    static int globalHeight = 600;
    
    static JFrame w = new JFrame("Log viewer");
    static JButton b_save = new JButton("Save log");
    static JTextArea ta_log = new JTextArea();
    
    public static void start()
    {
        Thread th = new Thread(new LogViewer());
        th.start();
    }
    
    @Override
    public void run()
    {
        WoliMC.log("INFO", "LogViewer", "Starting");
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
        b_save.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser jfc = new JFileChooser();
                int jfr = jfc.showSaveDialog(w);
                if (jfr == JFileChooser.APPROVE_OPTION)
                {
                    try {
                        FileWriter fw = new FileWriter(jfc.getSelectedFile().getPath());
                        for (int i = 0; i < WoliMC.logs.size(); i++)
                        {
                            fw.write(WoliMC.logs.get(i) + "\n");
                        }
                        fw.close();
                    } catch (IOException ioe) { WoliMC.log("ERROR", "LogViewer", ioe.toString()); }
                }
            }
        });
        draw(globalWidth, globalHeight);
        while (true)
        {
            try {
                resetLog();
                Thread.sleep(15);
            } catch (InterruptedException ioe) { WoliMC.log("ERROR", "LogViewer", ioe.toString()); }
        }
    }
    public static void draw(int width, int height)
    {
        globalWidth = width;
        globalHeight = height;
        
        w.setSize(globalWidth, globalHeight);
        
        b_save.setBounds(0, 0, 100, 25);
        ta_log.setBounds(0, 25, width, height - 25);
        
        w.add(b_save);
        w.add(ta_log);
        
        w.setLayout(null);
        w.setVisible(true);
    }
    public static void resetLog()
    {
        ta_log.setText("");
        for (int i = 0; i < WoliMC.logs.size(); i++)
        {
            ta_log.append(WoliMC.logs.get(i) + "\n");
        }
    }
}
