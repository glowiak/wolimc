package com.glowiak.wolimc.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import java.awt.Component;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.glowiak.wolimc.WoliMC;
import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.lang.Translations;

import com.glowiak.wolimc.auth.mojang.MojangAuth;
import com.glowiak.wolimc.auth.microsoft.MicrosoftAuth;

interface AWI
{
    JFrame w = new JFrame(Translations.getTranslation("Accounts"));
    JLabel text_accs = new JLabel(Translations.getTranslation("AccountSystem"));
    JRadioButton rb_mojang = new JRadioButton("Mojang");
    JRadioButton rb_ms = new JRadioButton("Microsoft");
    ButtonGroup bg = new ButtonGroup();
    JButton b_login = new JButton(Translations.getTranslation("Login"));
}

public class AccountsWindow implements AWI
{
    public static MojangAuth _mojangAuth = new MojangAuth();
    public static MicrosoftAuth _msAuth = new MicrosoftAuth();
    
    public static void run()
    {
        draw(275, 190);
        update();
        w.addComponentListener(new ComponentAdapter()
        {
            public void componentResized(ComponentEvent e)
            {
                Component c = (Component)e.getSource();
                draw(c.getWidth(), c.getHeight());
            }
        });
    }
    public static void draw(int width, int height)
    {
        w.setSize(width, height);
        
        text_accs.setBounds(5, 5, 275, 25);
        rb_mojang.setBounds(5, 30, 145, 25);
        rb_ms.setBounds(5, 55, 145, 25);
        b_login.setBounds(170, 30, width - 170, 20);
        
        bg.add(rb_mojang); bg.add(rb_ms);
        
        w.add(text_accs);
        w.add(rb_mojang); w.add(rb_ms);
        w.add(b_login);
        
        w.setLayout(null);
        w.setVisible(true);
    }
    public static void update()
    {
        if (Config.authSystem.equals("Mojang")) { rb_mojang.setEnabled(true); }
        else if (Config.authSystem.equals("Microsoft")) { rb_ms.setEnabled(true); }
    }
}
