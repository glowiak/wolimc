package com.glowiak.wolimc.instance;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.gui.WorldManager;
import com.glowiak.wolimc.Unzip;
import com.glowiak.wolimc.WoliMC;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;

public class Instance
{
    public static String prop_version;
    public static String prop_java;
    public static String prop_mem;
    public static String prop_nick;
    public static String prop_mainclass;
    
    public static int createInstance(String name, String version)
    {
        if (!new File(String.format("%s/instances", Config.getCwd())).exists())
        { Config.mkdir(String.format("%s/instances", Config.getCwd())); }
        
        if (new File(String.format("%s/instances/%s", Config.getCwd(), name)).exists())
        { WoliMC.log("ERROR", "Instance", "Instance already exists!"); return 1; } else
        {
            Config.mkdir(String.format("%s/instances/%s", Config.getCwd(), name));
            try {
                FileWriter fw = new FileWriter(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), name));
                
                Config.loadConfig();
                fw.write(String.format("%s\n", version));
                fw.write(String.format("%s\n", Config.setting_java));
                fw.write(String.format("%s\n", Config.setting_mem));
                fw.write(String.format("%s\n", Config.setting_nick));
                fw.write(String.format("%s\n", Version.mainClass));
                
                fw.close();
                
                new File(String.format("%s/instances/%s/jvmargs", Config.getCwd(), name)).createNewFile();
                return 0;
            } catch (IOException ioe) { System.out.println(ioe); return 1; }
        }
    }
    public static String getVersion(String instance)
    {
        if (!new File(String.format("%s/instances/%s", Config.getCwd(), instance)).exists())
        {
            return null;
        }
        try {
            FileReader fr = new FileReader(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), instance));
            BufferedReader br = new BufferedReader(fr);
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static String getJava(String instance)
    {
        if (!new File(String.format("%s/instances/%s", Config.getCwd(), instance)).exists())
        {
            return null;
        }
        try {
            FileReader fr = new FileReader(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), instance));
            BufferedReader br = new BufferedReader(fr);
            br.readLine();
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static String getMemory(String instance)
    {
        if (!new File(String.format("%s/instances/%s", Config.getCwd(), instance)).exists())
        {
            return null;
        }
        try {
            FileReader fr = new FileReader(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), instance));
            BufferedReader br = new BufferedReader(fr);
            for (int i = 0; i < 2; i++) { br.readLine(); }
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static String getNick(String instance)
    {
        if (!new File(String.format("%s/instances/%s", Config.getCwd(), instance)).exists())
        {
            return null;
        }
        try {
            FileReader fr = new FileReader(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), instance));
            BufferedReader br = new BufferedReader(fr);
            for (int i = 0; i < 3; i++) { br.readLine(); }
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static String getMainClass(String instance)
    {
        if (!new File(String.format("%s/instances/%s", Config.getCwd(), instance)).exists())
        {
            return null;
        }
        try {
            FileReader fr = new FileReader(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), instance));
            BufferedReader br = new BufferedReader(fr);
            for (int i = 0; i < 4; i++) { br.readLine(); }
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static String getJvmArgs(String instance)
    {
        if (!new File(String.format("%s/instances/%s", Config.getCwd(), instance)).exists())
        {
            return null;
        }
        try {
            FileReader fr = new FileReader(String.format("%s/instances/%s/jvmargs", Config.getCwd(), instance));
            BufferedReader br = new BufferedReader(fr);
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static void setJvmArgs(String instance, String jvmargs)
    {
        if (!new File(String.format("%s/instances/%s", Config.getCwd(), instance)).exists())
        {
            return;
        }
        try {
            FileWriter fw = new FileWriter(String.format("%s/instances/%s/jvmargs", Config.getCwd(), instance));
            fw.write(String.format("%s\n", jvmargs));
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static ArrayList<String> getInstanceList()
    {
        if (!new File(String.format("%s/instances", Config.getCwd())).exists()) { return null; }
        ArrayList<String> instances2 = new ArrayList<String>();
        File[] fbf = new File(String.format("%s/instances", Config.getCwd())).listFiles();
        for (File ft : fbf)
        {
            if (!basename(ft.toString()).equals("InstancesList"))
            { instances2.add(basename(ft.toString())); }
        }
        return instances2;
    }
    public static void deleteInstance(String name)
    {
        if (!new File(String.format("%s/instances", Config.getCwd())).exists()) { return; }
        
        WorldManager.rmdir(new File(String.format("%s/instances/%s", Config.getCwd(), name)));
    }
    public static boolean exists(String instance)
    {
        if (new File(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), instance)).exists()) { return true; }
        else { return false; }
    }
    public static int getPropertyLine(String property)
    {
        int wv = 1;
        switch(property)
        {
            case "version":
                wv = 0;
                break;
            case "java":
                wv = 1;
                break;
            case "mem":
                wv = 2;
                break;
            case "nick":
                wv = 3;
                break;
            case "main-class":
                wv = 4;
                break;
        }
        return wv;
    }
    public static void loadProps(String instance)
    {
        prop_version = getVersion(instance);
        prop_java = getJava(instance);
        prop_mem = getMemory(instance);
        prop_nick = getNick(instance);
        prop_mainclass = getMainClass(instance);
    }
    public static void setProperty(String inste, String property, String value)
    {
        loadProps(inste);
        try {
            FileWriter fw = new FileWriter(String.format("%s/instances/%s/instance.cfg", Config.getCwd(), inste));
            int settIndex = getPropertyLine(property);
            for (int i = 0; i < 5; i++)
            {
                if (i != settIndex && i == 0)
                { fw.write(String.format("%s\n", prop_version)); }
                if (i != settIndex && i == 1)
                { fw.write(String.format("%s\n", prop_java)); }
                if (i != settIndex && i == 2)
                { fw.write(String.format("%s\n", prop_mem)); }
                if (i != settIndex && i == 3)
                { fw.write(String.format("%s\n", prop_nick)); }
                if (i != settIndex && i == 4)
                { fw.write(String.format("%s\n", prop_mainclass)); }
                if (i == settIndex)
                {
                    fw.write(String.format("%s\n", value));
                }
            }
            fw.close();
        } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static String getSelected()
    {
        try {
            FileReader fr = new FileReader(String.format("%s/wolimc.cfg", Config.getCwd()));
            BufferedReader br = new BufferedReader(fr);
            
            for (int i = 0; i < 4; i++)
            {
                br.readLine();
            }
            
            return br.readLine();
        } catch (IOException ioe) { System.out.println(ioe); return null; }
    }
    public static String getAssetsDir(String instance)
    {
        return String.format("%s/assets/%s", Config.getCwd(), instance);
    }
    public static String basename(String fullpath)
    {
        return fullpath.substring(fullpath.lastIndexOf('/') + 1, fullpath.length());
    }
    public static boolean importInstance(String zip_path)
    {
        WoliMC.log("INFO", "Instance", String.format("Importing instance from %s", zip_path));
        if (!new File(zip_path).exists()) { return false; } else
        {
            if (!new File(String.format("%s/instances", Config.getCwd())).exists())
            { new File(String.format("%s/instances", Config.getCwd())).mkdir(); }
        
            try {
                Unzip.unzip(zip_path, String.format("%s/instances", Config.getCwd()));
            } catch (IOException ioe) { System.out.println(ioe); }
            return true;
        }
    }
}
