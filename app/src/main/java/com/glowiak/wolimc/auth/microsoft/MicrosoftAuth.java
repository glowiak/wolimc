package com.glowiak.wolimc.auth.microsoft;

import com.glowiak.wolimc.auth.Auth;

import com.glowiak.wolimc.config.Config;

public class MicrosoftAuth extends Auth
{
    public String authType = "Microsoft";
    
    public String authUrl = String.format("https://login.live.com/oauth20_authorize.srf?client_id=%s&response_type=code&redirect_uri=http://glowiak2.github.io/wolimc&scope=XboxLive.signin%20offline_access", Config.msaClientId);
    
    public MicrosoftAuth()
    {
        super();
    }
    public void login()
    {
    }
}
