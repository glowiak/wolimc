package com.glowiak.wolimc.auth;

import com.glowiak.wolimc.config.Config;

// abstract class for extending later

public abstract class Auth
{
    public String username;
    public String password;
    public String authType;
    
    public void login()
    {
    }
    public String getAuthType()
    {
        return authType;
    }
}
