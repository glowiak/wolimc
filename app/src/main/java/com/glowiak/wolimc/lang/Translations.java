package com.glowiak.wolimc.lang;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import com.glowiak.wolimc.config.Config;

public class Translations
{
    public static String getTranslation(String id)
    {
        try {
            FileReader fr = new FileReader(String.format("%s/language", Config.getCwd()));
            BufferedReader br = new BufferedReader(fr);
            Config.language = br.readLine();
            br.close();
            fr.close();
        } catch (IOException ioe) { Config.language = "english"; }
        String w = "";
        switch(id)
        {
            case "Version":
                if (Config.language.equals("english")) { w = "Version"; }
                else if (Config.language.equals("polski")) { w = "Wersja"; }
                else if (Config.language.equals("latina")) { w = "Versio"; }
                break;
            case "Accounts":
                if (Config.language.equals("english")) { w = "Accounts"; }
                else if (Config.language.equals("polski")) { w = "Konta"; }
                else if (Config.language.equals("latina")) { w = "Nominis"; }
                break;
            case "Kat":
                if (Config.language.equals("english")) { w = "Kat Kommander"; }
                else if (Config.language.equals("polski")) { w = "Koci Komandor"; }
                else if (Config.language.equals("latina")) { w = "Catti Imperator"; }
                break;
            case "Remove":
                if (Config.language.equals("english")) { w = "Remove"; }
                else if (Config.language.equals("polski")) { w = "Skasuj"; }
                else if (Config.language.equals("latina")) { w = "Remove"; }
                break;
            case "Launch":
                if (Config.language.equals("english")) { w = "Launch"; }
                else if (Config.language.equals("polski")) { w = "Uruchom"; }
                else if (Config.language.equals("latina")) { w = "Commit"; }
                break;
            case "Worlds":
                if (Config.language.equals("english")) { w = "Worlds"; }
                else if (Config.language.equals("polski")) { w = "Światy"; }
                else if (Config.language.equals("latina")) { w = "Terrae"; }
                break;
            case "Options":
                if (Config.language.equals("english")) { w = "Options"; }
                else if (Config.language.equals("polski")) { w = "Opcje"; }
                else if (Config.language.equals("latina")) { w = "Optionis"; }
                break;
            case "Mods":
                if (Config.language.equals("english")) { w = "Mods"; }
                else if (Config.language.equals("polski")) { w = "Mody"; }
                else if (Config.language.equals("latina")) { w = "Inflexionis"; }
                break;
            case "MCDir":
                if (Config.language.equals("english")) { w = "MC Dir"; }
                else if (Config.language.equals("polski")) { w = "Folder MC"; }
                else if (Config.language.equals("latina")) { w = "Derectorium MC"; }
                break;
            case "Settings":
                if (Config.language.equals("english")) { w = "Settings"; }
                else if (Config.language.equals("polski")) { w = "Ustawienia"; }
                else if (Config.language.equals("latina")) { w = "Occasus"; }
                break;
            case "InstancesFolder":
                if (Config.language.equals("english")) { w = "Instances Folder"; }
                else if (Config.language.equals("polski")) { w = "Folder Instalacji"; }
                else if (Config.language.equals("latina")) { w = "Derectorium Exempli"; }
                break;
            case "AddInstance":
                if (Config.language.equals("english")) { w = "Add Instance"; }
                else if (Config.language.equals("polski")) { w = "Dodaj Instalację"; }
                else if (Config.language.equals("latina")) { w = "Novum exemplum"; }
                break;
            case "ProperNick":
                if (Config.language.equals("english")) { w = "Enter a proper nick!"; }
                else if (Config.language.equals("polski")) { w = "Podaj poprawną nazwę!"; }
                else if (Config.language.equals("latina")) { w = "Imput nomen verum!"; }
                break;
            case "ProperJava":
                if (Config.language.equals("english")) { w = "Select a proper Java executable!"; }
                else if (Config.language.equals("polski")) { w = "Wybierz poprawny plik Javy!"; }
                else if (Config.language.equals("latina")) { w = "Opt iter verus Java!"; }
                break;
            case "WelcomeNew":
                if (Config.language.equals("english")) { w = "Welcome to WoliMC. This Wizard will let you set up basics."; }
                else if (Config.language.equals("polski")) { w = "Witaj w WoliMC. W tym oknie skonfigurujesz podstawowe opcje."; }
                else if (Config.language.equals("latina")) { w = "In WoliMC conplect. In haec luminare imput prior optionis."; }
                break;
            case "WIZARD_JAVA":
                if (Config.language.equals("english")) { w = "First, select Java, the runtime for Minecraft."; }
                else if (Config.language.equals("polski")) { w = "Po pierwsze, wybierz ścieżkę Javy, środowiska Majnkrafta."; }
                else if (Config.language.equals("latina")) { w = "Primus, imput iter Javae, machina Maincraphti."; }
                break;
            case "WIZARD_NICK":
                if (Config.language.equals("english")) { w = "Next, enter your Nick. It's the in-game name."; }
                else if (Config.language.equals("polski")) { w = "Po drugie, wpisz swoją nazwę. To nie powinno być Twoje imię"; }
                else if (Config.language.equals("latina")) { w = "Secundus, imput nomen tuum. In-ludus nomen est."; }
                break;
            case "WIZARD_MEMORY":
                if (Config.language.equals("english")) { w = "Last, specify the memory. More = better performance."; }
                else if (Config.language.equals("polski")) { w = "Na koniec, wybierz ile pamięci przydzielić grze."; }
                else if (Config.language.equals("latina")) { w = "Proxomus, imput memoria Maincraphti."; }
                break;
            case "WIZARD_THANKYOU":
                if (Config.language.equals("english")) { w = "Thank you for using WoliMC."; }
                else if (Config.language.equals("polski")) { w = "Dziękuję Ci za używanie WoliMC."; }
                else if (Config.language.equals("latina")) { w = "Ago ad usura WoliMC."; }
                break;
            case "WIZARD_TITLE":
                if (Config.language.equals("english")) { w = "Wizard"; }
                else if (Config.language.equals("polski")) { w = "Kreator Konfiguracji"; }
                else if (Config.language.equals("latina")) { w = "Incantator"; }
                break;
            case "Next":
                if (Config.language.equals("english")) { w = "Next"; }
                else if (Config.language.equals("polski")) { w = "Dalej"; }
                else if (Config.language.equals("latina")) { w = "Proximus"; }
                break;
            case "Browse":
                if (Config.language.equals("english")) { w = "Browse"; }
                else if (Config.language.equals("polski")) { w = "Przeglądaj"; }
                else if (Config.language.equals("latina")) { w = "Carp"; }
                break;
            case "Apply":
                if (Config.language.equals("english")) { w = "Apply"; }
                else if (Config.language.equals("polski")) { w = "Zastosuj"; }
                else if (Config.language.equals("latina")) { w = "Adtend"; }
                break;
            case "JavaPath":
                if (Config.language.equals("english")) { w = "Java path:"; }
                else if (Config.language.equals("polski")) { w = "Ścieżka Javy:"; }
                else if (Config.language.equals("latina")) { w = "Iter Javae:"; }
                break;
            case "Memory":
                if (Config.language.equals("english")) { w = "Memory:"; }
                else if (Config.language.equals("polski")) { w = "Pamięć:"; }
                else if (Config.language.equals("latina")) { w = "Memoria:"; }
                break;
            case "Metaserver":
                if (Config.language.equals("english")) { w = "Metaserver:"; }
                else if (Config.language.equals("polski")) { w = "Serwer metadanych:"; }
                else if (Config.language.equals("latina")) { w = "Credentarius metae:"; }
                break;
            case "Nick":
                if (Config.language.equals("english")) { w = "Nick:"; }
                else if (Config.language.equals("polski")) { w = "Nazwa:"; }
                else if (Config.language.equals("latina")) { w = "Nomen:"; }
                break;
            case "LilypadQA":
                w = "Lilypad QA ID:";
                break;
            case "Create":
                if (Config.language.equals("english")) { w = "Create"; }
                else if (Config.language.equals("polski")) { w = "Stwórz"; }
                else if (Config.language.equals("latina")) { w = "Face"; }
                break;
            case "Name":
                if (Config.language.equals("english")) { w = "Name:"; }
                else if (Config.language.equals("polski")) { w = "Nazwa:"; }
                else if (Config.language.equals("latina")) { w = "Nomen:"; }
                break;
            case "Version1":
                if (Config.language.equals("english")) { w = "Version:"; }
                else if (Config.language.equals("polski")) { w = "Wersja:"; }
                else if (Config.language.equals("latina")) { w = "Versio:"; }
                break;
            case "MainClass":
                if (Config.language.equals("english")) { w = "Main Class:"; }
                else if (Config.language.equals("polski")) { w = "Klasa główna:"; }
                else if (Config.language.equals("latina")) { w = "Ordo primum:"; }
                break;
            case "ClassWarn":
                if (Config.language.equals("english")) { w = "In 1.6+ change main class to 'net.minecraft.client.main.Main'"; }
                else if (Config.language.equals("polski")) { w = "W 1.6+ zmień klasę głowną na 'net.minecraft.client.main.Main'"; }
                else if (Config.language.equals("latina")) { w = "In 1.6+ commut ordo primum 'net.minecraft.client.main.Main'"; }
                break;
            case "ReplaceWarn":
                if (Config.language.equals("english")) { w = "These settings override the defaults."; }
                else if (Config.language.equals("polski")) { w = "Te opcje nadpisują domyślne."; }
                else if (Config.language.equals("latina")) { w = "Haec optionis commutt defaltae."; }
                break;
            case "JVMArgs":
                if (Config.language.equals("english")) { w = "JVM Args:"; }
                else if (Config.language.equals("polski")) { w = "Argumenty JVM:"; }
                else if (Config.language.equals("latina")) { w = "Argumenti Javae:"; }
                break;
            case "InstallModloader":
                if (Config.language.equals("english")) { w = "Install ModLoader"; }
                else if (Config.language.equals("polski")) { w = "Zainstaluj ModLoadera"; }
                else if (Config.language.equals("latina")) { w = "Adjic Modloaderi"; }
                break;
            case "InstallForge":
                if (Config.language.equals("english")) { w = "Install Forge"; }
                else if (Config.language.equals("polski")) { w = "Zainstaluj Forga"; }
                else if (Config.language.equals("latina")) { w = "Adjic Forgi"; }
                break;
            case "InstallFabric":
                if (Config.language.equals("english")) { w = "Install Fabric"; }
                else if (Config.language.equals("polski")) { w = "Zainstaluj Fabrica"; }
                else if (Config.language.equals("latina")) { w = "Adjic Fabrici"; }
                break;
            case "AddToJar":
                if (Config.language.equals("english")) { w = "Add to jar"; }
                else if (Config.language.equals("polski")) { w = "Dodaj do jara"; }
                else if (Config.language.equals("latina")) { w = "Adjic in jarm"; }
                break;
            case "AddMod":
                if (Config.language.equals("english")) { w = "Add mod"; }
                else if (Config.language.equals("polski")) { w = "Dodaj moda"; }
                else if (Config.language.equals("latina")) { w = "Adjic inflexio"; }
                break;
            case "RemoveMod":
                if (Config.language.equals("english")) { w = "Remove mod"; }
                else if (Config.language.equals("polski")) { w = "Usuń moda"; }
                else if (Config.language.equals("latina")) { w = "Remove inflexio"; }
                break;
            case "UseAutoClass":
                if (Config.language.equals("english")) { w = "Detect main class"; }
                else if (Config.language.equals("polski")) { w = "Znajdź klasę główną"; }
                else if (Config.language.equals("latina")) { w = "Reperi ordi prima"; }
                break;
            case "Import":
                if (Config.language.equals("english")) { w = "Import"; }
                else if (Config.language.equals("polski")) { w = "Importuj"; }
                else if (Config.language.equals("latina")) { w = "Infer"; }
                break;
            case "Kill":
                if (Config.language.equals("english")) { w = "Kill"; }
                else if (Config.language.equals("polski")) { w = "Zabij"; }
                else if (Config.language.equals("latina")) { w = "Interfice"; }
                break;
            case "Export":
                if (Config.language.equals("english")) { w = "Export"; }
                else if (Config.language.equals("polski")) { w = "Eksportuj"; }
                else if (Config.language.equals("latina")) { w = "Export"; }
                break;
            case "Language":
                if (Config.language.equals("english")) { w = "Language:"; }
                else if (Config.language.equals("polski")) { w = "Język:"; }
                else if (Config.language.equals("latina")) { w = "Lingua"; }
                break;
            case "INVALID_NAME":
                if (Config.language.equals("english")) { w = "Invalid instance name!"; }
                else if (Config.language.equals("polski")) { w = "Niepoprawna nazwa instalacji!"; }
                else if (Config.language.equals("latina")) { w = "Nomen exempli corruptum est!"; }
                break;
            case "AccountSystem":
                if (Config.language.equals("english")) { w = "Account System:"; }
                else if (Config.language.equals("polski")) { w = "System kont:"; }
                else if (Config.language.equals("latina")) { w = "Machina nomeni:"; }
                break;
            case "Login":
                if (Config.language.equals("english")) { w = "Login"; }
                else if (Config.language.equals("polski")) { w = "Zaloguj się"; }
                else if (Config.language.equals("latina")) { w = "Signa"; }
                break;
        }
        return w;
    }
}
