package com.glowiak.wolimc.launch;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.launch.Classpath;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.Unzip;
import com.glowiak.wolimc.ZipUtil;
import com.glowiak.wolimc.WoliMC;
import com.glowiak.wolimc.gui.WorldManager;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;

public class Launch implements Runnable
{
    public static Process p;
    static String _instance = "";
    
    public Launch(String instance)
    {
        _instance = instance;
    }
    
    public static void startInstance(String instance)
    {
        Launch _launch = new Launch(instance);
        Thread th = new Thread(_launch);
        th.start();
    }
    
    @Override
    public void run()
    {
        String instance = _instance;
        Config.loadConfig();
        String version = Instance.getVersion(instance);
        String java = Instance.getJava(instance);
        String memory = Instance.getMemory(instance);
        String nick = Instance.getNick(instance);
        String mainClass = Instance.getMainClass(instance);
        
        WoliMC.log("INFO", "Launch", "Extracting natives");
        Classpath.extractNatives(instance);
        
        // a1.0.16.05 patches
        if (Instance.getVersion(instance).equals("a1.0.16.05-preview")) {
            if (new File(String.format("%s/preview_data", Config.getCwd())).exists()) // for some reason, the .05 files are
            { new File(String.format("%s/preview_data", Config.getCwd())).delete(); } // generating in launcher rootdir instead of .minecraft
        }
        
        String doExec;
        if (Instance.getJvmArgs(instance) != null) {
            doExec = String.format("%s -Xmn%s -Xmx%s -XX:+UseConcMarkSweepGC -Dfile.encoding=UTF-8 -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Djava.library.path=%s/instances/%s/natives -Dminecraft.launcher.brand=WoliMC -Dminecraft.launcher.version=%s -cp %s%s %s %s %s --username %s --version %s --userProperties {} --gameDir %s --assetsDir %s --assetIndex %s --accessToken 12 --userType legacy", java, memory, memory, Config.getCwd(), instance, String.format("%d.%d.%d", Config.WoliMC_VERSION_MAJOR, Config.WoliMC_VERSION_MINOR, Config.WoliMC_VERSION_HOTFIX), Classpath.build_classpath(), String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), instance), Instance.getJvmArgs(instance), mainClass, nick, nick, version, String.format("%s/instances/%s/.minecraft", Config.getCwd(), instance), String.format("%s/assets/%s", Config.getCwd(), instance), version);
        } else {
            doExec = String.format("%s -Xmn%s -Xmx%s -XX:+UseConcMarkSweepGC -Dfile.encoding=UTF-8 -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Djava.library.path=%s/instances/%s/natives -Dminecraft.launcher.brand=WoliMC -Dminecraft.launcher.version=%s -cp %s%s %s %s --username %s --version %s --userProperties {} --gameDir %s --assetsDir %s --assetIndex %s --accessToken 12 --userType legacy", java, memory, memory, Config.getCwd(), instance,String.format("%d.%d.%d", Config.WoliMC_VERSION_MAJOR, Config.WoliMC_VERSION_MINOR, Config.WoliMC_VERSION_HOTFIX), Classpath.build_classpath(), String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), instance), mainClass, nick, nick, version, String.format("%s/instances/%s/.minecraft", Config.getCwd(), instance), String.format("%s/assets/%s", Config.getCwd(), instance), version);
        }
        
        WoliMC.log("INFO", "Launch", String.format("Running command: %s", doExec));
        
        try { p = Runtime.getRuntime().exec(doExec); } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static void addToJar(String jar, String dest_jar, String toAdd)
    {
        if (new File(String.format("%s/tmp-jar", Config.getCwd())).exists())
        { new File(String.format("%s/tmp-jar", Config.getCwd())).mkdir(); }
        
        if (!new File(jar).exists()) { return; }
        if (!new File(toAdd).exists()) { return; }
        
        try {
            Unzip.unzip(jar, String.format("%s/tmp-jar", Config.getCwd()));
            Unzip.unzip(toAdd, String.format("%s/tmp-jar", Config.getCwd()));
            
            if (new File(dest_jar).exists()) { new File(dest_jar).delete(); }
            ZipUtil.mkZip(String.format("%s/tmp-jar", Config.getCwd()), dest_jar);
            
            WorldManager.rmdir(new File(String.format("%s/tmp-jar", Config.getCwd())));
        } catch (IOException ioe) { System.out.println(ioe); }
    }
}
