package com.glowiak.wolimc.launch;

import com.glowiak.wolimc.DownloadFile;
import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.instance.Instance;
import com.glowiak.wolimc.launch.Version;
import com.glowiak.wolimc.Unzip;
import com.glowiak.wolimc.WoliMC;
import java.net.URL;
import java.io.File;
import java.net.MalformedURLException;
import java.io.IOException;

public class Classpath
{
    public static void createJarDir()
    {
        if (!new File(String.format("%s/instances/%s/jars", Config.getCwd(), Config.setting_selinst)).exists())
        { Config.mkdir(String.format("%s/instances/%s/jars", Config.getCwd(), Config.setting_selinst)); }
    }
    public static void fetchJars(String instance)
    {
        Config.loadConfig();
        createJarDir();
        for (int i = 0; i < Version.gameJars_189.length; i++)
        {
            if (!new File(String.format("%s/instances/%s/jars/%s", Config.getCwd(), instance, getFileFromUrl(Version.gameJars_189[i]))).exists())
            {
                DownloadFile.fetch(Version.gameJars_189[i], String.format("%s/instances/%s/jars/%s", Config.getCwd(), instance, Version.gameJars_189[i].substring(Version.gameJars_189[i].lastIndexOf('/') + 1, Version.gameJars_189[i].length())));
            }
        }
        DownloadFile.fetch(Version.getClientUrl(Instance.getVersion(instance)), String.format("%s/instances/%s/minecraft.jar", Config.getCwd(), instance));
    }
    public static String build_classpath()
    {
        String w = "";
        for (int i = 0; i < Version.gameJars_189.length; i++)
        {
            w = String.format("%s:%s", String.format("%s/instances/%s/jars/%s", Config.getCwd(), Config.setting_selinst, getFileFromUrl(Version.gameJars_189[i])), w);
        }
        return w;
    }
    public static String getFileFromUrl(String url)
    {
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }
    public static void extractNatives(String instance)
    {
        Config.loadConfig();
        if (!new File(String.format("%s/instances/%s/natives", Config.getCwd(), instance)).exists())
        { new File(String.format("%s/instances/%s/natives", Config.getCwd(), instance)).mkdir(); }
        
        // extract lwjgl
        WoliMC.log("INFO", "Classpath", "Extracting lwjgl-2.9.4-nightly-20150209");
        try {
            Unzip.unzip(String.format("%s/instances/%s/jars/lwjgl-platform-2.9.4-nightly-20150209-natives-%s.jar", Config.getCwd(), instance, getOS()),
                        String.format("%s/instances/%s/natives", Config.getCwd(), instance));
        } catch (IOException ioe) { WoliMC.log("ERROR", "Classpath", ioe.toString()); }
        
        // extract jinput
        WoliMC.log("INFO", "Classpath", "Extracting jinput-2.0.5");
        try {
            Unzip.unzip(String.format("%s/instances/%s/jars/jinput-platform-2.0.5-natives-%s.jar", Config.getCwd(), instance, getOS()),
                        String.format("%s/instances/%s/natives", Config.getCwd(), instance));
        } catch (IOException ioe) { WoliMC.log("ERROR", "Classpath", ioe.toString()); }
    }
    public static String getOS()
    {
        String w = "";
        String wv = System.getProperty("os.name");
        if (wv.contains("indow")) { w = "windows"; }
        if (wv.contains("inu")) { w = "linux"; }
        if (wv.contains("SD")) { w = "linux"; }
        
        return w;
    }
}
