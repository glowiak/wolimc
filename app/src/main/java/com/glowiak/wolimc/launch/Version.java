package com.glowiak.wolimc.launch;

import com.glowiak.wolimc.config.Config;
import com.glowiak.wolimc.DownloadFile;
import com.glowiak.wolimc.WoliMC;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class Version
{
    public static String mainClass = "net.minecraft.launchwrapper.Launch";
    
    public static String[] gameJars_189 = {
        "https://libraries.minecraft.net/com/mojang/netty/1.8.8/netty-1.8.8.jar",
        "https://libraries.minecraft.net/oshi-project/oshi-core/1.1/oshi-core-1.1.jar",
        "https://libraries.minecraft.net/net/java/dev/jna/jna/3.4.0/jna-3.4.0.jar",
        "https://libraries.minecraft.net/net/java/dev/jna/platform/3.4.0/platform-3.4.0.jar",
        "https://libraries.minecraft.net/com/ibm/icu/icu4j-core-mojang/51.2/icu4j-core-mojang-51.2.jar",
        "https://libraries.minecraft.net/net/sf/jopt-simple/jopt-simple/4.6/jopt-simple-4.6.jar",
        "https://libraries.minecraft.net/com/paulscode/codecjorbis/20101023/codecjorbis-20101023.jar",
        "https://libraries.minecraft.net/com/paulscode/codecwav/20101023/codecwav-20101023.jar",
        "https://libraries.minecraft.net/com/paulscode/libraryjavasound/20101123/libraryjavasound-20101123.jar",
        "https://libraries.minecraft.net/com/paulscode/librarylwjglopenal/20100824/librarylwjglopenal-20100824.jar",
        "https://libraries.minecraft.net/com/paulscode/soundsystem/20120107/soundsystem-20120107.jar",
        "https://libraries.minecraft.net/io/netty/netty-all/4.0.23.Final/netty-all-4.0.23.Final.jar",
        "https://libraries.minecraft.net/com/google/guava/guava/17.0/guava-17.0.jar",
        "https://libraries.minecraft.net/org/apache/commons/commons-lang3/3.3.2/commons-lang3-3.3.2.jar",
        "https://libraries.minecraft.net/commons-io/commons-io/2.4/commons-io-2.4.jar",
        "https://libraries.minecraft.net/commons-codec/commons-codec/1.9/commons-codec-1.9.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput/2.0.5/jinput-2.0.5.jar",
        "https://libraries.minecraft.net/net/java/jutils/jutils/1.0.0/jutils-1.0.0.jar",
        "https://libraries.minecraft.net/com/google/code/gson/gson/2.2.4/gson-2.2.4.jar",
        "https://libraries.minecraft.net/com/mojang/authlib/1.5.21/authlib-1.5.21.jar",
        "https://libraries.minecraft.net/com/mojang/realms/1.7.59/realms-1.7.59.jar",
        "https://libraries.minecraft.net/org/apache/commons/commons-compress/1.8.1/commons-compress-1.8.1.jar",
        "https://libraries.minecraft.net/org/apache/httpcomponents/httpclient/4.3.3/httpclient-4.3.3.jar",
        "https://libraries.minecraft.net/commons-logging/commons-logging/1.1.3/commons-logging-1.1.3.jar",
        "https://libraries.minecraft.net/org/apache/httpcomponents/httpcore/4.3.2/httpcore-4.3.2.jar",
        "https://libraries.minecraft.net/org/apache/logging/log4j/log4j-api/2.0-beta9/log4j-api-2.0-beta9.jar",
        "https://libraries.minecraft.net/org/apache/logging/log4j/log4j-core/2.0-beta9/log4j-core-2.0-beta9.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl/2.9.4-nightly-20150209/lwjgl-2.9.4-nightly-20150209.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl_util/2.9.4-nightly-20150209/lwjgl_util-2.9.4-nightly-20150209.jar",
        "https://libraries.minecraft.net/net/minecraft/launchwrapper/1.5/launchwrapper-1.5.jar",
        "https://libraries.minecraft.net/java3d/vecmath/1.3.1/vecmath-1.3.1.jar",
        "https://libraries.minecraft.net/net/sf/trove4j/trove4j/3.0.3/trove4j-3.0.3.jar",
        "https://libraries.minecraft.net/org/ow2/asm/asm-all/4.1/asm-all-4.1.jar",
        "https://libraries.minecraft.net/argo/argo/2.25_fixed/argo-2.25_fixed.jar",
        
        // natives
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.4-nightly-20150209/lwjgl-platform-2.9.4-nightly-20150209.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.4-nightly-20150209/lwjgl-platform-2.9.4-nightly-20150209-natives-linux.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.4-nightly-20150209/lwjgl-platform-2.9.4-nightly-20150209-natives-osx.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.4-nightly-20150209/lwjgl-platform-2.9.4-nightly-20150209-natives-windows.jar",
        
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-linux.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-osx.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-windows.jar",
        
        "https://libraries.minecraft.net/tv/twitch/twitch/6.5/twitch-6.5.jar",
        "https://libraries.minecraft.net/tv/twitch/twitch-platform/6.5/twitch-platform-6.5-natives-osx.jar",
        "https://libraries.minecraft.net/tv/twitch/twitch-platform/6.5/twitch-platform-6.5-natives-windows-32.jar",
        "https://libraries.minecraft.net/tv/twitch/twitch-platform/6.5/twitch-platform-6.5-natives-windows-64.jar",
        "https://libraries.minecraft.net/tv/twitch/twitch-external-platform/4.5/twitch-external-platform-4.5-natives-windows-32.jar",
        "https://libraries.minecraft.net/tv/twitch/twitch-external-platform/4.5/twitch-external-platform-4.5-natives-windows-64.jar" };
    
    public static int getProtocol(String version)
    {
        int w = 0;
        switch(version)
        {
            // ALPHA
            case "a1.0": w = 1; break;
            case "a1.0.1_01": w = 2; break;
            case "a1.0.2_01": w = 3; break;
            case "a1.0.2_02": w = 4; break;
            case "a1.0.3": w = 5; break;
            case "a1.0.4": w = 6; break;
            case "a1.0.5": w = 7; break;
            case "a1.0.5_01": w = 8; break;
            case "a1.0.6": w = 9; break;
            case "a1.0.6_01": w = 10; break;
            case "a1.0.6_03": w = 11; break;
            case "a1.0.7": w = 12; break;
            case "a1.0.8_01": w = 13; break;
            case "a1.0.9": w = 14; break;
            case "a1.0.10": w = 15; break;
            case "a1.0.11": w = 16; break;
            case "a1.0.12": w = 17; break;
            case "a1.0.13": w = 18; break;
            case "a1.0.13_01": w = 19; break;
            case "a1.0.14": w = 20; break;
            case "a1.0.15": w = 21; break;
            case "a1.0.16": w = 22; break;
            case "a1.0.16_01": w = 23; break;
            case "a1.0.16_02": w = 24; break;
            case "a1.0.16.05-preview": w = 25; break;
            case "a1.0.16.05-unrpreview2": w = 26; break;
            case "a1.0.16.05-lilypad_qa": w = 27; break;
            case "a1.0.17_02": w = 28; break;
            case "a1.0.17_03": w = 29; break;
            case "a1.0.17_04": w = 30; break;
            case "a1.1.0": w = 31; break;
            case "a1.1.1": w = 32; break;
            case "a1.1.2": w = 33; break;
            case "a1.1.2_01": w = 34; break;
            case "a1.2.0": w = 35; break;
            case "a1.2.0_01": w = 36; break;
            case "a1.2.0_02": w = 37; break;
            case "a1.2.1_01": w = 38; break;
            case "a1.2.2a": w = 39; break;
            case "a1.2.2b": w = 40; break;
            case "a1.2.3": w = 41; break;
            case "a1.2.3_01": w = 42; break;
            case "a1.2.3_02": w = 43; break;
            case "a1.2.3_04": w = 44; break;
            case "a1.2.3_05": w = 45; break;
            case "a1.2.4_01": w = 46; break;
            case "a1.2.5": w = 47; break;
            case "a1.2.6": w = 48; break;
            
            // BETA
            case "b1.0": w = 49; break;
            case "b1.2": w = 50; break;
            case "b1.3": w = 51; break;
            case "b1.4": w = 52; break;
            case "b1.4_01": w = 53; break;
            case "b1.5": w = 54; break;
            case "b1.5_01": w = 55; break;
            case "b1.6-tb3": w = 56; break;
            case "b1.6": w = 57; break;
            case "b1.6.6": w = 58; break;
            case "b1.7": w = 59; break;
            case "b1.7.3": w = 60; break;
            case "b1.8-pre": w = 61; break;
            case "b1.8-pre2": w = 62; break;
            case "b1.8": w = 63; break;
            case "b1.8.1": w = 64; break;
            
            // RELEASE
            case "1.0": w = 65; break;
            case "1.1": w = 66; break;
            case "1.2": w = 67; break;
            case "1.2.1": w = 68; break;
            case "1.2.5": w = 69; break;
            case "1.3": w = 70; break;
            case "1.3.1": w = 71; break;
            case "1.4": w = 72; break;
            case "1.4.2": w = 73; break;
            case "1.4.4": w = 74; break;
            case "1.4.6": w = 75; break;
            case "1.5": w = 76; break;
            case "1.5.2": w = 77; break;
            case "1.6": w = 78; break;
            case "1.6.4": w = 79; break;
            case "1.7.2": w = 80; break;
            case "1.7.4": w = 81; break;
            case "1.7.5": w = 82; break;
            case "1.7.6": w = 83; break;
            case "1.7.7": w = 84; break;
            case "1.7.8": w = 85; break;
            case "1.7.9": w = 86; break;
            case "1.7.10": w = 87; break;
            case "1.8": w = 88; break;
        }
        return w;
    }
    public static void updateVersionManifest()
    {
        WoliMC.log("INFO", "Version", "Updating the manifest");
        Config.loadConfig();
        
        if (new File(String.format("%s/versions.txt", Config.getCwd())).exists())
        { new File(String.format("%s/versions.txt", Config.getCwd())).delete(); }
        
        DownloadFile.fetch(String.format("%s/VersionsByProtocol.txt", Config.setting_meta), String.format("%s/versions.txt", Config.getCwd()));
        DownloadFile.fetch(String.format("%s/VersionsByName.txt", Config.setting_meta), String.format("%s/version_names.txt", Config.getCwd()));
        DownloadFile.fetch(String.format("%s/objects.txt", Config.setting_meta), String.format("%s/objects.txt", Config.getCwd()));
    }
    public static String getClientUrl(String version)
    {
        if (!new File(String.format("%s/versions.txt", Config.getCwd())).exists())
        { updateVersionManifest(); }
        
        String w = "";
        
        try {
            FileReader fr = new FileReader(String.format("%s/versions.txt", Config.getCwd()));
            BufferedReader br = new BufferedReader(fr);
            
            int i = 0;
            for (String bpt = br.readLine(); bpt != null; bpt = br.readLine())
            {
                i++;
                if (i == getProtocol(version))
                { w = bpt; }
            }
            br.close();
            fr.close();
        } catch (IOException ioe) { System.out.println(ioe); w = null; }
        
        return w;
    }
    public static String getAutoClass(String version)
    {
        if (getProtocol(version) >= 78)
        {
            return "net.minecraft.client.main.Main";
        } else
        {
            return "net.minecraft.launchwrapper.Launch";
        }
    }
}
