package com.glowiak.wolimc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.net.URL;

public class DownloadFile
{
    public static void fetch(String url, String dst)
    {
        try { fetch1(url, dst); } catch (IOException ioe) { System.out.println(ioe); }
    }
    public static void fetch1(String url, String dst) throws IOException
    {
        File ie = new File(dst);
        if (!ie.exists()) {
            WoliMC.log("INFO", "DownloadFile", String.format("Downloading %s", url));
            URL u1 = new URL(url);
            Path u2 = Paths.get(dst);
            InputStream is = u1.openStream();
            try {
                Files.copy(is, u2, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ioe) { System.out.println(ioe); }
        }
    }
}
